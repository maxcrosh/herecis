import React from "react"

import {
  Button,
  Container,
  Row,
  Col,
  Card
} from "reactstrap"

import CardsFooter from "../components/CardsFooter"
import NavigationBar from '../components/NavigationBar'
import { Reason } from "../components/Reason"
import SliderReasons from "../components/learning/SliderReasons"


import reason_img from '../assets/img/learning/team.jpg'
import participate_img from '../assets/img/learning/why_participate_3.svg'
import programm_schedule from '../assets/img/learning/programm_schedule.svg'

export const TeachersProgramm = props => {
  
  const badge_list = [
      { title: "Обучение «мягким» навыкам: self-менеджмент на каждый день, методологии управления проектами, мастерство ведения переговоров, подготовка презентаций, развитие бренда личности", icon: "fa fa-users" },
      { title: "Изучение платформ: HERE Studio, Map Creator", icon: "fa fa-globe" },
      { title: "Опыт взаимодействия с людьми из разных городов и стран", icon: "fa fa-map-o" },
      { title: "Погружение в рабочую атмосферу компании HERE Technologies", icon: "fa fa-bullhorn" },
  ]
  
  return (
    <>
      <NavigationBar />
        <main>
            <div className="position-relative">
              {/* shape Hero */}
              <section className="section section-lg section-shaped pb-250">
                <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require('assets/img/learning/main.jpg')})`}}>
                  <span />
                  <span />
                  <span />
                  <span />
                  <span />
                  <span />
                  <span />
                  <span />
                  <span />
                </div>
                <Container className="py-lg-md d-flex">
                  <div className="col px-0">
                    <Row>
                      <Col lg="8">
                        <h1 className="display-5 text-white page-title" style={{marginBottom: "100px"}}>
                          Программа для преподавателей{" "}
                          <span></span>
                        </h1>
                          <Button
                            className="mb-3 mb-sm-0"
                            href="https://forms.office.com/Pages/ResponsePage.aspx?id=zTRAbSVyck-4U5H-rqZJGegRNy8qPdhNvb6IK_zIuyRUQzQwR1lTWU9JVURZM0M4RjZFR0tCNlY0NC4u"
                            target="_blank"
                            color="here-da"
                          >
                          <span className="btn-inner--text">Подать заявку</span>
                          </Button>
                      
                      </Col>
                    </Row>
                  </div>
                </Container>
                {/* SVG separator */}
                <div className="separator separator-bottom separator-skew">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      preserveAspectRatio="none"
                      version="1.1"
                      viewBox="0 0 2560 100"
                      x="0"
                      y="0"
                    >
                      <polygon
                        className="fill-white"
                        points="2560 0 2560 100 0 100"
                      />
                    </svg>
                  </div>
                </section>
                {/* 1st Hero Variation */}
              </div>
              <Reason
                  icon="ni ni-hat-3"
                  title="Что такое «Программа для преподавателей от HERE Technologies»?"
                  description={
                      `Программа подготовлена с целью, обучить преподавателей комплексу «мягких» навыков и познакомить с сервисами компании,
                       а также помочь узнать больше о направлениях развития современных геотехнологий. 
                       Итогом участия является создание своей собственной интерактивной карты с помощью сервиса HERE Studio.
                      `
                  }
                  img={reason_img}
                  badge_title="Возможности:"
                  badge_list={badge_list}
              />
              
              {/* <section className="section section-lg  bg-gradient-here-da">
                <Container>
                  <Row className="row-grid align-items-center">
                      <Col md="12">
                        <img
                            alt="..."
                            className="img-fluid"
                            src={participate_img}
                        />
                      </Col>                  
                  </Row>
                </Container>
            </section> */}
            <section className="section section-lg pt-10">
                <Container>
                  <Row className="row-grid align-items-center">
                      <Col md="12">
                        <img
                            alt="..."
                            className="img-fluid"
                            src={programm_schedule}
                        />
                      </Col>                  
                  </Row>
                </Container>
            </section>
            <section className="section section-lg">
              <SliderReasons />
            </section>
              
            <section className="section section-lg pt-10">
            <Container>
              <Card className="bg-gradient-here-da shadow-lg border-0">
                <div className="p-5">
                  <Row className="align-items-center">
                    <Col lg="8">
                      <h3 className="text-white">
                        Присоединяйтесь к программе!
                      </h3>
                      <p className="lead text-white mt-3">
                        Заполните форму участника и мы свяжемся с Вами!
                      </p>
                    </Col>
                    <Col className="ml-lg-auto" lg="3">
                      <Button
                        block
                        className="btn-white"
                        color="default"
                        target="_blank"
                        href="https://forms.office.com/Pages/ResponsePage.aspx?id=zTRAbSVyck-4U5H-rqZJGegRNy8qPdhNvb6IK_zIuyRUQzQwR1lTWU9JVURZM0M4RjZFR0tCNlY0NC4u"
                        size="lg"
                      >
                        Подать заявку
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Container>
          </section>
        </main>
      <CardsFooter />
    </>
  )
}

export default TeachersProgramm
