import React from "react"


import {
  Button,
  Container,
  Row,
  Col,
  Card,
  CardTitle,
  CardBody,
} from "reactstrap"


import NavigationBar from '../components/NavigationBar'
import CardsFooter from "../components/CardsFooter"


import project_medical from '../assets/img/here-studio/projects/project-medical-analysis.png'
import project_minsk_tourism from '../assets/img/here-studio/projects/project-minsk-tourism.png'
import project_skfu_stv from '../assets/img/here-studio/projects/project-skfu-stv.png'
import project_minsk_flats from '../assets/img/here-studio/projects/project-minsk-flats.png'
import project_nn_child from '../assets/img/here-studio/projects/project-nn-child.png'
import project_nn_help from '../assets/img/here-studio/projects/project-nn-help.png'
import project_yar_medical from '../assets/img/here-studio/projects/project-yar-medical.png'
import project_yar_green from '../assets/img/here-studio/projects/project-yar-green.png'

export class ProjectsLayout extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      projects: [
        {
          img: project_medical,
          title: "Нижний Новгород",
          description: "Анализ работы скорой медицинской помощи в Нижнем Новгороде с помощью изохронов",
          link: "https://studio.here.com/viewer/?project_id=f67c9385-ea3b-4762-93b6-b198b326a0f9"
        },
        {
          img: project_minsk_flats,
          title: "Минск",
          description: "Соотношение цен посуточной аренды домов отдыха в пределах временной доступности от центра города",
          link: "https://studio.here.com/viewer/?project_id=a6947baf-cb81-4b23-bc5c-5883dd4fcbfb"
        },
        {
          img: project_yar_green,
          title: "Ярославль",
          description: "Выявление зеленых зон и определение предполагаемых зеленых зон для благоустройства города",
          link: "https://studio.here.com/viewer/?project_id=74bd48d9-8d8e-43b9-af45-8ab587ca5c18"
        },
        {
          img: project_minsk_tourism,
          title: "Минск",
          description: "Туристический сервис помогающий проанализировать отели, кафе, достопримечательности ... ",
          link: "https://studio.here.com/viewer/?project_id=7cbae3ad-8e8d-4c84-b060-451bc8e1e4a7"
        },
        {
          img: project_skfu_stv,
          title: "Ставрополь",
          description: "Сети заведений общественного питания на карте. С разделением по уровню доступности для студентов",
          link: "https://studio.here.com/viewer/?project_id=3585d73d-d555-425e-a23d-16a3d082b2aa"
        },
        {
          img: project_nn_child,
          title: "Нижний Новгород",
          description: "Карта для родителей и их детей с расположение детских садов, школ, больниц, кружков, детских площадок",
          link: "https://studio.here.com/viewer/?project_id=2429a774-596f-4263-b64a-288f14651bc7"
        },
        {
          img: project_nn_help,
          title: "Нижний Новгород",
          description: "Создание комфортной городской среды для людей с ограниченными возможностями",
          link: "https://studio.here.com/viewer/?project_id=f7d2311b-e0a8-420b-9816-d631994d4ef6"
        },
        {
          img: project_yar_medical,
          title: "Ярославль",
          description: "Обеспечение города пунктами продаж лекарственных средств и учреждений медицинского обслуживания",
          link: "https://studio.here.com/viewer/?project_id=ef295bc0-b3a3-4524-a194-a2b5df11a1b6"
        },
        
      ]
    }
  
  }

  render() {

    let { projects } = this.state

    return (
      <>
        <NavigationBar />
        <main ref="main">
          <div className="position-relative">
            <section className="section section-lg section-shaped pb-200">
              <div className="shape shape-style-3 shape-here" style={{backgroundImage: `url(${require('assets/img/here-studio/background-map-2.png')})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                    <Col lg="6">
                      <h1 className="display-3 text-white">
                        HERE Studio{" "}
                        <span></span>
                      </h1>
                      <p className="lead text-white" style={{fontWeight: '500'}}>
                        Облачная геолокационная платформа для обработки, управления и визуализации геоданных
                      </p>
                      <div className="btn-wrapper">
                        <Button
                          className="btn-icon mb-3 mb-sm-0"
                          color="here-oa"
                          href="https://studio.here.com/"
                          target="_blank"
                        >
                          <span className="btn-inner--icon mr-1">
                            <i className="fa fa-code" />
                          </span>
                          <span className="btn-inner--text">Создать карту</span>
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
            </div>
        
            <section className="section section-lg pt-lg-0 mt--200">
                <Container>
                    <Row className="justify-content-center">
                        <Col lg="12">
                            <Row className="row-grid">
                                {
                                  projects.map( (project, key) => {
                                    return <Col key={key} lg="4" md="6" sm="12"  className="mt-5">
                                              <Card className="card-lift--hover shadow border-0">
                                                  <CardTitle className="p-0 m-0">
                                                      <img src={project.img} className="w-100"></img>
                                                  </CardTitle>
                                                  <CardBody className="py-3">
                                                      <h6 className="text-uppercase">{project.title}</h6>
                                                      <p className="description mt-3">{project.description}</p>
                                                    
                                                      <a href={project.link} target="_blank">
                                                          <Button
                                                              className="mt-4"
                                                              color="here-da-outline"
                                                          >
                                                              Открыть карту
                                                          </Button>
                                                      </a>
                                                  </CardBody>
                                              </Card>
                                          </Col>
                                  })
                                }
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </section>
          </main>
        <CardsFooter />
      </>
    )
  }
}

export default ProjectsLayout
