import React, { Component } from "react"
import Slider from "react-slick"

import {
    Card,
    CardImg
} from 'reactstrap'

export default class PricesSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    };
    return (
      <div>
        <Slider {...settings}>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/tshirt.JPG")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/raincover.JPG")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0">
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/loudspeaker.JPG")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/certificate.jpg")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/bottle.jpg")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/charger.JPG")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/termocup.JPG")}
                    top
                />
            </Card>
            <Card className="bg-white shadow border-0"  >
                <CardImg
                    alt="..."
                    src={require("assets/img/internship/prices/2022/jacket.JPG")}
                    top
                />
            </Card>
        </Slider>
      </div>
    );
  }
}