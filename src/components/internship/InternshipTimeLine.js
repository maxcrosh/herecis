import React, { Component } from 'react'

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component'


import {
    Container,
    Card,
} from 'reactstrap'

const contentStyle = {
    background: '#85e0ce', 
    color: '#fff'
}

const contentArrowStyle = {
    borderRight: '7px solid  #85e0ce'
}

const iconStyle = {
    background: '#fff', 
    color: '#fff', 
    border:"13px solid #85e0ce"
}

class TimeLine extends Component {
    render () {
        return (
            <section className="section section-lg  pb-100">
                <Container>
                    <Card style={{border: "0", borderBottom: "4px solid #85e0ce"}}>
                        <div className="p-5">
                            <h2 className="display-2  text-center">
                                Этапы участия
                            </h2>
                        </div>
                    </Card>
                    <VerticalTimeline style={{border: "0", borderBottom: "4px solid #85e0ce"}}>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={contentStyle}
                            contentArrowStyle={contentArrowStyle}
                            iconStyle={iconStyle}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Основной этап</h3>
                            <p>
                                Вы вносите полезные изменения в картографическом редакторе Map Creator.
                                Специалисты HERE проверяют и отслеживают количество и качество внесённых вами данных. 
                                Если возникают вопросы, мы активно вам помагаем их решить. Этап длится до 25 мая 2020.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={contentStyle}
                            contentArrowStyle={contentArrowStyle}
                            iconStyle={iconStyle}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Отборочный тур</h3>
                            <p>
                                С 25 мая по 25 июня - награждение участников брендированными призами и отбор на стажировку в компании HERE Technologies.
                                Выбор участников будет базироваться на результах редактирования в Map Creator – качество и количество изменений.  
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={contentStyle}
                            contentArrowStyle={contentArrowStyle}
                            iconStyle={iconStyle}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Финал</h3>
                            <p>
                                С 20 июля по 10 августа участники, которые проявили себя, получат возможность пройти стажировку в офисе компании.<br/>
                                С вами проведут предварительное собеседование и выдадут подготовительные материалы. 
                            </p>
                        </VerticalTimelineElement>
                    </VerticalTimeline>
                </Container>    
            </section>
        )
    }
}

export default TimeLine