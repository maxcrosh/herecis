import React, { Component } from 'react'

import { 
    Button, 
    Container, 
    Row, 
    Col, 
    UncontrolledTooltip,
    Card,
    CardImg,
} from "reactstrap"

class Blog extends Component {
    render () {
        return (
            <>
            <section className="section section-lg">
                <Container>
                    <Row className="row-grid justify-content-center">
                        <Col className="text-center" lg="8">
                            <div className="text-center">
                                <h2 className="display-3">
                                    Как создать приложение{" "}
                                    <span className="text-here-da">
                                        с помощью HERE API/SDK или Studio?
                                    </span>
                                </h2>
                                <p className="lead">
                                В нашем блоге можно узнать о применении современных 
                                технологий совместно с геосервисами HERE Technologies
                                </p>
                                <Row className="justify-content-center">
                                    <Col lg="2" xs="4">
                                        <a
                                            href="https://blog.herecis.ru/category/python/"
                                            id="tooltip255035741"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/img/blog/python-logo-200x200.png")}
                                            />
                                        </a>
                                        <UncontrolledTooltip delay={0} target="tooltip255035741">
                                            Статьи с использованием Python
                                        </UncontrolledTooltip>
                                    </Col>
                                    <Col lg="2" xs="4">
                                        <a
                                            href="https://blog.herecis.ru/category/android/"
                                            id="tooltip265846671"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/img/blog/android-logo-200x200.jpg")}
                                            />
                                        </a>
                                        <UncontrolledTooltip delay={0} target="tooltip265846671">
                                            Создание Android приложений на основе HERE Android SDK
                                        </UncontrolledTooltip>
                                    </Col>
                                    <Col lg="2" xs="4">
                                        <a
                                            href="https://blog.herecis.ru/category/javascript/"
                                            id="tooltip233150499"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/img/blog/js-logo-200x200.png")}
                                            />
                                        </a>
                                        <UncontrolledTooltip delay={0} target="tooltip233150499">
                                            Интерактивных карт с HERE JavaScript Maps API 
                                        </UncontrolledTooltip>
                                    </Col>
                                    <Col lg="2" xs="4">
                                        <a
                                            href="https://blog.herecis.ru/"
                                            id="tooltip308866163"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/img/blog/arduino-logo-200x200.png")}
                                            />
                                        </a>
                                        <UncontrolledTooltip delay={0} target="tooltip308866163">
                                            Визуализация данных с датчиков на карте
                                        </UncontrolledTooltip>
                                    </Col>
                                    <Col lg="2" xs="4">
                                        <a
                                            href="https://blog.herecis.ru/category/here-xyz/"
                                            id="tooltip76119384"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/img/blog/xyz-logo.png")}
                                            />
                                        </a>
                                        <UncontrolledTooltip delay={0} target="tooltip76119384">
                                            Быстрое создание геоприложений с HERE Studio
                                        </UncontrolledTooltip>
                                    </Col>
                                </Row>
                                
                                <div className="btn-wrapper">
                                    <Button
                                        className="mb-3 mb-sm-0 mt-5"
                                        target="_blank"
                                        color="here-da"
                                        href="https://blog.herecis.ru/"
                                        rel="noopener noreferrer"
                                    >
                                        Перейти на блог
                                    </Button>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div className="mt-5">
                    <Container className="container-lg">
                        <Row>
                            <Col className="mb-5 mb-md-0" md="4">
                                <Card className="card-lift--hover shadow border-0">
                                    <a  
                                        target="_blank" 
                                        rel="noopener noreferrer"
                                        href="https://blog.herecis.ru/android-interactive-map/"
                                    >
                                        <CardImg
                                           
                                            alt="..."
                                            src={require("assets/img/blog/android-app.jpg")}
                                        />
                                    </a>
                                </Card>
                            </Col>
                            <Col className="mb-5 mb-lg-0" md="4">
                                <Card className="card-lift--hover shadow border-0">
                                    <a  
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        href="https://blog.herecis.ru/mappin-xyz-sport-activity/"
                                    >
                                        <CardImg
                                           
                                            alt="..."
                                            src={require("assets/img/blog/xyz-sport.jpg")}
                                        />
                                    </a>
                                </Card>
                            </Col>
                            <Col className="mb-5 mb-lg-0" md="4">
                                <Card className="card-lift--hover shadow border-0">
                                    <a  
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        href="https://blog.herecis.ru/isoline-christmas/"
                                    >
                                        <CardImg
                                           
                                            alt="..."
                                            src={require("assets/img/blog/isoline.jpg")}
                                        />
                                    </a>                              
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </section>
            </>
        )
    }
}

export default Blog