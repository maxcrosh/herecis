import React, { Component } from 'react'

import {
    Card,
    CardBody,
    Container,
    Row,
    Col,
} from 'reactstrap'


class University extends Component {
    render () {
        return (
            <section className="section pb-0 bg-gradient-here-da">
                <Container>
                    <Row className="row-grid  align-items-center">
                    <Col lg="6">
                            <div className="d-flex px-3">
                                <div>
                                    <div className="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle">
                                        <i className="ni ni-paper-diploma text-here-da" />
                                    </div>
                                </div>
                                <div className="pl-4">
                                    <h4 className="display-4 text-white">HERE University</h4>
                                    <p className="text-white">
                                        В рамках сотрудничества с университетами и компаниями, мы проводим специальные треннинги.
                                        При желании каждый может пройти уроки онлайн. 
                                    </p>
                                </div>
                            </div>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-2">
                                                <i className="fa fa-code" />
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                                Воркшопы WEB/Mobile
                                            </h5>
                                            <p>
                                                Проведение образовательных воркшопов по разработке геолокационных приложений для Android 
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                <div className="d-flex px-3">
                                    <div>
                                        <div className="icon icon-shape here-da-2">
                                            <i className="fa fa-user-o" />
                                        </div>
                                    </div>
                                    <div className="pl-4">
                                        <h5 className="title text-here-da">
                                            Мастер классы
                                        </h5>
                                        <p>
                                            Публичный выступления, управление проектами, работа с геоинформационными системами и многое другое можно узнать на мастер классах от сотрудников HERE Technologies
                                        </p>
                                    </div>
                                </div>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="6">
                            <Card className="shadow shadow-lg--hover">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-2">
                                                <i className="fa fa-desktop" />
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                                Онлайн уроки
                                            </h5>
                                            <p>
                                                Набор интерактивных уроков по созданию приложений и работе в Map Creator доступны онлайн
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-2">
                                                <i className="fa fa-video-camera" />
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                                Вебинары
                                            </h5>
                                            <p>
                                               Проведение вебинаров по разработке веб и мобильных приложений, а также консультации по геолокационными сервисам
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
                {/* SVG separator */}
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="none"
                        version="1.1"
                        viewBox="0 0 2560 100"
                        x="0"
                        y="0"
                    >
                        <polygon
                        className="fill-white"
                        points="2560 0 2560 100 0 100"
                        />
                    </svg>
                </div>
            </section>
        )
    }
}

export default University