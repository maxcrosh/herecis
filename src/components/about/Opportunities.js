import React, { Component } from 'react'

import { Link } from "react-router-dom"

import {
    Badge,
    Card,
    CardBody,
    Button,
    Container,
    Row,
    Col,
  } from "reactstrap"

class Opportunities extends Component {
    render () {
        return (
            <section className="section section-lg pt-lg-0 mt--200">
                <Container>
                    <Row className="row-grid">
                        <Col lg="6">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                    <div className="icon icon-shape here-oa rounded-circle mb-4">
                                        <i className="ni ni-building" />
                                    </div>
                                    <h6 className="text-here-oa text-uppercase">
                                        Community Challenge #5
                                    </h6>
                                    <p className="description mt-3">
                                        Хочешь получить практический опыт работы в международной компании?
                                        Развивайся вместе с нами. 
                                    </p>
                                    <div>
                                        <Badge pill className="mr-1 badge-here-oa">
                                            Активное участие в проектах компании
                                        </Badge>
                                        <Badge pill className="mr-1 badge-here-oa">
                                            Знание ГИС и картографии
                                        </Badge>
                                        <br/>
                                        <Badge pill className="mr-1 badge-here-oa">
                                            Желание получить опыт
                                        </Badge>
                                    </div>
                                    <Link to="/internship">
                                        <Button
                                            className="mt-4"
                                            color="here-oa-outline"
                                        >
                                            Подробней
                                        </Button>
                                    </Link>
                                </CardBody>
                            </Card>
                        </Col>
                        {/* <Col lg="4">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                <div className="icon icon-shape here-da rounded-circle mb-4">
                                    <i className="ni ni-circle-08" />
                                </div>
                                <h6 className="text-here-da text-uppercase">
                                    HERE Data Engineering
                                </h6>
                                <p className="description mt-3">
                                    Онлайн программа по сбору, анализу и визуализации пространственных данных
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </p>
                                <div>
                                    <Badge pill className="mr-1 badge-here-da">
                                        Сотрудничество с университетами
                                    </Badge>
                                    <Badge pill className="mr-1 badge-here-da">
                                        Опыт работы в Map Creator и HERE Studio
                                    </Badge>
                                </div>
                                <Link to="/learning" target="_blank">
                                    <Button className="mt-4" color="here-da-outline"> Подробней </Button>
                                </Link>
                                
                                </CardBody>
                            </Card>
                        </Col> */}
                        <Col lg="6">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                <div className="icon icon-shape here-yg rounded-circle mb-4">
                                    <i className="ni ni-circle-08" />
                                </div>
                                <h6 className="text-here-yg text-uppercase">
                                    Стать амбассадором
                                </h6>
                                <p className="description mt-3">
                                    Амбассадоры выступают для сообщества, поддерживают текущие проекты, реализуют инициативы
                                    <br/>
                                    <br/>
                                </p>
                                <div>
                                    <Badge pill className="mr-1 badge-here-yg">
                                        Сотрудничество с университетами
                                    </Badge>
                                    <Badge pill className="mr-1 badge-here-yg">
                                        Опыт работы в Map Creator и HERE Studio
                                    </Badge>
                                    {/* <Badge pill className="mr-1 here-yg-2">
                                        Навыки проведения презентаций
                                    </Badge> */}
                                </div>
                                <Link to="/ambassador" target="_blank">
                                    <Button className="mt-4" color="here-yg-2"> Подробней </Button>
                                </Link>
                                
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Opportunities