import React, { Component, useState  } from "react"

import {
  Container,
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from "reactstrap"


import step_1 from '../../assets/img/learning/step_1.png'
import step_2 from '../../assets/img/learning/step_2.png'
import step_3 from '../../assets/img/learning/step_3.png'
import step_4 from '../../assets/img/learning/step_4.png'


import './style.css'

const SliderReasons = (props) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const [animating, setAnimating] = useState(false)

  const items = [
    {
      src: step_1,
      caption: '',
    },
    {
      src: step_2,
      caption: '',
    },
    {
      src: step_3,
      caption: '',
    },
    {
      src: step_4,
      caption: '',
    },
  ]
  

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img style={{maxWidth: "950px"}} src={item.src}/>
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    )
  })

  return (
    <Container>
      <Row className="text-center justify-content-center">
        <Col lg="12">
          <Carousel
            activeIndex={activeIndex}
            next={next}
            previous={previous}
            interval={false}
          >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
            {slides}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous}/>
            <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
          </Carousel>
        </Col>
      </Row>
    </Container>
  )
}
export default SliderReasons 

// class SliderReasons extends Component {
//   render() {
    
//     return (
//       <Container>
//           <Row className="text-center justify-content-center">
//               <Col lg="12">
//                   <Carousel items={items}/>
//               </Col>
//           </Row>
//       </Container>
//     )
//   }
// }