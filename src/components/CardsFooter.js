import React from "react";

import {
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap"

class CardsFooter extends React.Component {
  render() {
    return (
      <>
        <footer className="footer ">
          <Container>
            <Row className="row-grid align-items-center my-md">
              <Col lg="6">
                <h3 className="font-weight-light mb-2 text-here-da">
                  Спасибо за то что вы с нами!
                </h3>
                <h4 className="mb-0 font-weight-light">
                  Давайте оставаться на связи
                </h4>
              </Col>
              
            </Row>
            <hr />
            <Row className="align-items-center justify-content-md-between">
              <Col md="6">
                <div className="copyright">
                  © {new Date().getFullYear()}{" "}
                  <a
                    href="https://www.here.com"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="text-here-da"
                  >
                    HERE Technologies
                  </a>
                </div>
              </Col>
              <Col md="6">
                <Nav className="nav-footer justify-content-end">
                  <NavItem>
                    <NavLink
                      href="https://github.com/heremaps"
                      target="_blank"
                    >
                      Github
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://www.youtube.com/user/heremaps"
                      target="_blank"
                    >
                      Youtube
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://twitter.com/here?s=20"
                      target="_blank"
                    >
                      Twitter
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://www.instagram.com/here/?hl=en"
                      target="_blank"
                    >
                      Instagram
                    </NavLink>
                  </NavItem>
                </Nav>
              </Col>
            </Row>
          </Container>
        </footer>
      </>
    );
  }
}

export default CardsFooter
