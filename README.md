# HERE CIS Portal

Web site for HERE CIS community of developers and cartographers.

## Installation

1. Check if you have NodeJS and npm installed on you computer:

```bash
npm -v
```

2. Go to folder with cloned project and install requirements:

```bash
npm install
```

3. Start local development server:

```bash
npm start
```

4. Great! Your are ready to work.
